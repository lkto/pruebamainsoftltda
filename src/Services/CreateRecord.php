<?php

namespace App\Services;

use App\Entity\Record;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class CreateRecord
{

    /**
     * @var ApiClient
     */
    private $apiClient;
    /**
     * @var Location
     */
    private $location;
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(Location $location, EntityManagerInterface $entityManager)
    {
        $this->location = $location;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function handle(Record $request): Record
    {
        $codeCity = $request->getCity()->getCode() . ',' . $request->getCity()->getCounty()->getCodeIso();
        $location = $this->location->get($codeCity);
        $humidity = $location->main->humidity;
        $latitude = $location->coord->lat;
        $longitude = $location->coord->lon;
        $record = new Record();
        $record->setCity($request->getCity());
        $record->setHumidity($humidity);
        $record->setLatitude($latitude);
        $record->setLongitude($longitude);
        $record->setCreatedAt(new \DateTime());
        $this->entityManager->persist($record);
        $this->entityManager->flush();
        return $record;
    }

}