<?php

namespace App\Services;

use phpDocumentor\Reflection\Types\Object_;

class Location
{
    private $urlLocation = 'http://api.openweathermap.org/geo/1.0/direct';
    private $urlWeather = 'https://api.openweathermap.org/data/2.5/weather';
    private $appId = '2854f121a6b4fe7d12ffc9a45d27ace4';

    public function get(string $codeCity)
    {
        $clientInformation = $this->getClientInformation($codeCity);
        return $this->getWeatherInformation($clientInformation[0]);
    }

    public function getClientInformation(string $codeCity)
    {
        $url = $this->urlLocation . "?q=".$codeCity."&limit=5&appid=".$this->appId;
        return $this->getDataApi($url);
    }

    public function getWeatherInformation ($client)
    {
        $url = $this->urlWeather . "?lat=".$client->lat."&lon=".$client->lon."&appid=".$this->appId;
        return $this->getDataApi($url);
    }

    public function getDataApi(string $url)
    {
        $options = [
            CURLOPT_HEADER => false,
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false
        ];

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response);
    }
}