<?php

namespace App\Entity;

use App\Repository\GalleryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GalleryRepository::class)
 */
class Gallery
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $link;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $artist;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $categoryMusic;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getArtist(): ?int
    {
        return $this->artist;
    }

    public function setArtist(?int $artist): self
    {
        $this->artist = $artist;

        return $this;
    }

    public function getCategoryMusic(): ?int
    {
        return $this->categoryMusic;
    }

    public function setCategoryMusic(?int $categoryMusic): self
    {
        $this->categoryMusic = $categoryMusic;

        return $this;
    }
}
