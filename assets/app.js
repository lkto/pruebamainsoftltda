/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';
import  './styles/assets/vendor/glightbox/css/glightbox.min.css'
import  './styles/assets/vendor/remixicon/remixicon.css'
import  './styles/assets/vendor/swiper/swiper-bundle.min.css'
import  './styles/assets/css/style.css'

import  './styles/assets/vendor/glightbox/js/glightbox.min.js'
import  './styles/assets/vendor/isotope-layout/isotope.pkgd.min.js'
import './styles/assets/js/main.js'







import $ from 'jquery';

global.$ = global.jQuery = $;

// start the Stimulus application
import './bootstrap';
import 'datatables/media/js/jquery.dataTables';

$('#data-table').dataTable();